module Application
where

import Imports

import Database.Persist            as P
import Database.Persist.Postgresql
    (ConnectionPool, ConnectionString, PostgresConf (..), runMigration)
import Database.Persist.Sql        (SqlPersistT)
import Database.V5.Bloodhound      (MappingName (..))


import Import.Exceptions
import ModelTH



testClient :: ClientDb
testClient = ClientDb "Test client"

mapping :: MappingName
mapping = MappingName "doc"

getPGConnStr :: ConnectionString
getPGConnStr = "dbname=tram host=127.0.0.1 user=vlatko password=pero port=5432"

newtype AppT m a = AppT {unAppT :: ReaderT App m a}
  deriving (Functor, Applicative, Monad)

data App = App { appPG    :: PGConfig
               , appParse :: TestConfig
               }

data PGConfig = PGConfig { pgConf :: PostgresConf
                         , pgPool :: ConnectionPool
                         }

data TestConfig = TestConfig { tcName :: Text, tcSomeBool :: Bool}
type AppM       = ExceptT AppException (ReaderT App IO)
type DbAction a = SqlPersistT IO a

mkApp :: IO App
mkApp = do
  pgConfig    <- initSQL $ PostgresConf getPGConnStr 1 1 10
  parseConfig <- return $ TestConfig {tcName = "pero", tcSomeBool = True}

  let app = App { appPG          = pgConfig
                , appParse       = parseConfig
                }
  return app


initSQL :: PostgresConf -> IO PGConfig
initSQL pc = do
  pool <- createPoolConfig pc
  return $ PGConfig{pgConf = pc, pgPool = pool}


runApp :: App -> AppM a -> IO (Either AppException a)
runApp app a = do
  void $ runReaderT (runExceptT go) app
  runReaderT (runExceptT a) app
  where
    go = runDB $ do
      runMigration migrateAll
      entityKey <$> upsertBy (UniqueClientName $ clientDbName testClient) testClient []

getApp :: AppM App
getApp = getsApp id

getsApp :: (App -> a) -> AppM a
getsApp = asks

runDB :: DbAction b -> AppM b
runDB a = do
  PGConfig{..} <- getsApp appPG
  liftIO $ runPool pgConf a pgPool

runDBwCfg :: PGConfig -> DbAction b -> IO b
runDBwCfg PGConfig{..} a = runPool pgConf a pgPool

runDBRT :: Exception e => DbAction (Either e b) -> AppM  b
runDBRT a = do
  PGConfig{..} <- getsApp appPG
  rethrow =<< liftIO (runPool pgConf a pgPool)

rethrow :: (Exception e, MonadCatch m) => Either e a -> m a
rethrow = either throw pure

runDBEither :: DbAction a -> AppM (Either AppException a)
runDBEither query = do
  PGConfig{..} <- lift $ asks appPG
  liftIO $ tryApp $ runPool pgConf query pgPool
  where
    tryApp :: (MonadCatch m) => m a -> m (Either AppException a)
    tryApp = try

getByNF :: (MonadIO m, PersistRecordBackend record backend, PersistUniqueRead backend
           , MonadThrow m)
        => Unique record -> ReaderT backend m (Entity record)
getByNF u = getBy u >>= \case
  Nothing -> throw ExcNotFound
  Just a  -> return a

getNF :: (MonadIO m, PersistRecordBackend record backend, PersistStoreRead backend
         , MonadThrow m)
        => Key record -> ReaderT backend m record
getNF r = P.get r >>= \case
  Nothing -> throw ExcNotFound
  Just a  -> return a
