{-# LANGUAGE TemplateHaskell #-}
module Backend.DB
  ( ClientDb (..)
  , ClientDbId
  , UserId
  , UniqueIdDbId
  , TransUnitDbId
  , TmxDbId
  , TmxDb

  , dbStoreTmx
  , dbStoreTransUnit
  , dbReadTmx
  , dbReadTransUnits

  )
where

import Imports

import Database.Persist  as P

import Application
import Import.Exceptions
import ModelTH
import Tmx.Parser


dbStoreTmx :: Text -> TmxCore -> DbAction (Either AppException (Key TmxDb))
dbStoreTmx cNm x@TmxCore{..} = runExceptT $ do
  cId <- lift (fmap entityKey <$> getBy (UniqueClientName cNm)) !!? ExcClientNotFound
  lift (fmap entityVal <$> getBy (UniqueTmxTitle cId tmxTitle) >>= \case
          Just xDb -> Just <$> ups (xDb{tmxDbContent = encode x})
          Nothing  -> do
            uId <- P.insert (UniqueIdDb cId)
            Just <$> P.insert (TmxDb tmxTitle cId uId (encode x))
       ) !!? ExcTmxNoStoreOrUpdate
  where
    ups r = entityKey <$> upsertBy (UniqueTmxUniq $ tmxDbUniqId r) r []

dbStoreTransUnit :: Key TmxDb -> TransUnitCore -> DbAction (Key TransUnitDb)
dbStoreTransUnit xId f@TransUnitCore{..} = do
  fId <- ups $ TransUnitDb xId tuId (showAttr tuSrcLang) $ encode f
  return fId
  where
    ups r = entityKey <$> upsertBy (UniqueTransUnitParId xId tuId) r []


dbReadTmx :: Key TmxDb -> DbAction ([AppException], TmxCore)
dbReadTmx xId = do
  tmx <- either (throw . ExcDecode ("Tmx: " <> tshow xId))
                 pure . decode . tmxDbContent
             =<< getNF xId
  return ([], tmx)

dbReadTransUnits :: Key TmxDb -> DbAction ([AppException], [TransUnitCore])
dbReadTransUnits xId =
  partitionEithers . map go <$> P.selectList [TransUnitDbParent ==. xId] [LimitTo 10]
  where
    go (Entity fId TransUnitDb{..}) =
      either (Left . ExcDecode ("File: " <> tshow fId))
             Right
           $ decode transUnitDbContent
