{-# LANGUAGE DeriveGeneric #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Backend.ES
where

import Imports                hiding (encode)

import Control.Monad.Fail     (MonadFail (..))
import Data.Aeson             (defaultOptions, genericParseJSON, genericToJSON)
import Data.ByteString.Base64 as B64
import Data.Text.Encoding     as Enc
import Database.V5.Bloodhound (BH)

import Tmx.Parser             (Lang (..), readAttr, showAttr)

instance MonadFail (BH IO) where fail = error


------------------------------------------------------------------------------------------
-- Datatype & mapping
------------------------------------------------------------------------------------------

data TmxMapping = TmxMapping deriving (Eq, Show)
data SupportedLang = En | De | Hr
  deriving (Show, Eq)


instance ToJSON TmxMapping where
  toJSON TmxMapping =
    object [ "properties" .=
      object
        [ "tmx".= object [keywordType]
        , langMapp En
        , langMapp De
        , langMapp Hr
        ]
    ]

data TransUnitDoc = TransUnitDoc
  { tudTmx :: Text
  , tudEN  :: Maybe TransUnitVar
  , tudDE  :: Maybe TransUnitVar
  , tudHR  :: Maybe TransUnitVar
  } deriving (Eq, Generic, Show)


langMapp :: SupportedLang -> Pair
langMapp sl = do
  let analyzer = case sl of
        En -> "analyzer" .= ("standard" :: Text)
        De -> "analyzer" .= ("standard" :: Text)
        Hr -> "analyzer" .= ("standard" :: Text)
  toLower (tshow sl) .=
    object ["properties" .=
      object [ "estuLRegion" .= object [keywordType]
             , "estuLScript" .= object [keywordType]
             , "estuTU"      .= object [textType, analyzer]
             , "estuTags"    .= doNotIndex
             , "estuUsed"    .= object [intType] -- maybe doNotIndex
             ]
           ]



data TransUnitVar = TransUnitVar
  { tuvLRegion :: Maybe Text
  , tuvLScript :: Maybe Text
  , tuvContent :: Text
  , tuvTags    :: ByteString
  , tuvUsed    :: Int
  } deriving (Eq, Generic, Show)




------------------------------------------------------------------------------------------
-- Instances (orphan)
------------------------------------------------------------------------------------------

instance ToJSON   TransUnitVar where toJSON    = genericToJSON defaultOptions
instance FromJSON TransUnitVar where parseJSON = genericParseJSON defaultOptions
instance ToJSON   TransUnitDoc where toJSON    = genericToJSON defaultOptions
instance FromJSON TransUnitDoc where parseJSON = genericParseJSON defaultOptions
instance ToJSON   Lang         where toJSON l  = String $ showAttr l
instance FromJSON Lang         where
  parseJSON (String l) = maybe mempty pure $ readAttr l
  parseJSON _          = mempty
instance FromJSON ByteString where
  parseJSON (String t) = pure . either mempty id . B64.decode $ Enc.encodeUtf8 t
  parseJSON _          = empty
instance ToJSON ByteString where toJSON = String . Enc.decodeUtf8 . B64.encode

------------------------------------------------------------------------------------------
-- Utils
------------------------------------------------------------------------------------------
textType, keywordType, intType :: Pair
textType    = "type" .= ("text"    :: Text)
keywordType = "type" .= ("keyword" :: Text)
intType     = "type" .= ("integer" :: Text)

doNotIndex :: Value
doNotIndex = object ["enabled" .= False]
