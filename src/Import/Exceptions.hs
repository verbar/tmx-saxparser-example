{-# LANGUAGE DeriveAnyClass #-}
module Import.Exceptions where

import Imports

data AppException =
    ExcText [Text]
  | ExcNotFound
  | ExcBadKeyConversion
  | ExcClientNotFound
  | ExcTmxNoStoreOrUpdate
  | ExcDecode Text PeekException
  deriving (Show, Eq)
  deriving anyclass (Exception)
