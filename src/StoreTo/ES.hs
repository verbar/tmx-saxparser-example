module StoreTo.ES
  ( EsState
  , parseTmxToEs

  , f1
  , esMain
  )
where

import Imports                        as Imp

import Data.Aeson                     as Aeson (eitherDecode, encode)
import Database.V5.Bloodhound
import Network.HTTP.Client            (Response (..), defaultManagerSettings)
import Network.HTTP.Types             (Status (..))


import Application                    (AppM, mkApp, runApp, mapping)
import Backend.ES                     (TmxMapping (..), TransUnitDoc (..),
                                       TransUnitVar (..))
import Import.Exceptions              (AppException)
import Text.XML.SAXCombinators        (IdxNode)
import Text.XML.SAXCombinators.Hexpat ()
import Tmx.Parser                     (Lang (..), PError, Tmx (..), TmxState, TmxUnknown,
                                       TransUnitCore (..), TransUnitVarCore (..),
                                       UniqTmxId, replaceTags, streamTmxUnkTo, tmxTag)


type EsState = TmxState (Maybe Int)

f1 :: IO (Either AppException (Either PError EsState))
f1 = do
  app <- mkApp

  let ids = defaultIndexDocumentSettings
      inFnm = "data/Test-doc2.tmx"
      uId = 1
      idx = IndexName "tmx_es"
  res <- runApp app (parseTmxToEs inFnm uId ids idx)
  return res

parseTmxToEs :: FilePath -> UniqTmxId -> IndexDocumentSettings -> IndexName
             -> AppM (Either PError EsState)
parseTmxToEs inFnm uId ids idx = liftIO $ do
  runBH' $ do
    respOK "delete idx" =<< deleteIndex idx
    respOK "create idx" =<< createIndex (IndexSettings (ShardCount 1) (ReplicaCount 0)) idx
    respOK "putMapping" =<< putMapping idx mapping TmxMapping

  (streamTmxUnkTo out (pack inFnm) uId =<< readEvents inFnm) >>= \case
    Left err ->
      -- putStrLn ("Error: " <> tshow err) >>
      pure (Left  err)
    Right s  ->
      -- putStrLn ("State: " <> tshow s)   >>
      pure (Right s)

  where
    runBH' = withBH defaultManagerSettings testServer
    out :: EsState -> (TmxUnknown,[IdxNode]) -> IO (Either PError EsState)
    out (g,_s) (unk, _unknowns) = runBH' $ do
      case tmxTag unk of
        TmxTransUnit TransUnitCore{..} -> do
          let replTag isEnd i           = "<" <> bool "" "/" isEnd <> tshow i <> ">"
              anyLang                   = Lang "*" "" "" []
              mkTU TransUnitVarCore{..} = do
                let (tuC, tags) = replaceTags replTag tuvContent
                    (Lang{..})  = either (const anyLang) id tuvXmlLang
                ( toLower langLanguage
                 , TransUnitVar
                     { tuvLRegion = bool Nothing (Just $ pack langRegion) $ null langRegion
                     , tuvLScript = bool Nothing (Just $ pack langScript) $ null langScript
                     , tuvContent = tuC
                     , tuvTags    = Imp.encode tags
                     , tuvUsed    = 1
                     }
                 )
              tuvs = toList $ map mkTU tuTUVs
              mainTU = TransUnitDoc { tudTmx = "FromTMX"
                                    , tudEN  = lookup "en" tuvs
                                    , tudDE  = lookup "de" tuvs
                                    , tudHR  = lookup "hr" tuvs
                                    }
          void $ indexDocument idx mapping ids mainTU (DocId $ fst tuId)
        _ -> pure ()
      Right . (g,) . Just <$> liftIO getRandomID

resetIndex :: BH IO ()
resetIndex = do
  respOK "delete idx" =<< deleteIndex testIndex
  respOK "create idx" =<< createIndex (IndexSettings (ShardCount 1) (ReplicaCount 0)) testIndex
  respOK "putMapping" =<< putMapping testIndex mapping TmxMapping


testIndex :: IndexName
testIndex = IndexName "tmx"
testServer :: Server
testServer = Server "http://localhost:9200"


varEn, varHr, varDe :: TransUnitVar
varEn = TransUnitVar (Just "US") Nothing "Text in English."      "" 0
varHr = TransUnitVar (Just "HR") Nothing "Tekst na hrvatskom."   "" 0
varDe = TransUnitVar (Just "DE") Nothing "Text ist in Englisch." "" 0

tuAll, tuEn, tuHrDe, tuDeEn :: TransUnitDoc
tuAll  = TransUnitDoc {tudTmx = "MyTMX", tudEN = Just varEn, tudDE = Just varDe, tudHR = Just varHr}
tuEn   = TransUnitDoc {tudTmx = "MyTMX", tudEN = Just varEn, tudDE = Nothing,    tudHR = Nothing}
tuHrDe = TransUnitDoc {tudTmx = "MyTMX", tudEN = Nothing,    tudDE = Just varDe, tudHR = Just varHr}
tuDeEn = TransUnitDoc {tudTmx = "MyTMX", tudEN = Just varEn, tudDE = Just varDe, tudHR = Nothing}


esMain :: IO ()
esMain = runBH' $ do
  -- set up index
  resetIndex

  -- index TUs
  showResp "TUAll"  =<< indexDocument testIndex mapping defaultIndexDocumentSettings tuAll  (DocId "All")
  showResp "TUEn"   =<< indexDocument testIndex mapping defaultIndexDocumentSettings tuEn   (DocId "En")
  showResp "TUHrDe" =<< indexDocument testIndex mapping defaultIndexDocumentSettings tuHrDe (DocId "HrDe")
  showResp "TUDeEn" =<< indexDocument testIndex mapping defaultIndexDocumentSettings tuDeEn (DocId "DeEn")
  void $ refreshIndex testIndex


  let _enUS = Lang{ langLanguage = "en", langScript = "", langRegion = "US", langVariants = []}
      _hrHR = Lang{ langLanguage = "hr", langScript = "", langRegion = "HR", langVariants = []}
      _deDE = Lang{ langLanguage = "de", langScript = "", langRegion = "DE", langVariants = []}

      sent = "hrvatski"

  print $ Aeson.encode $ simpleTUSearch _hrHR sent
  resp <- searchByIndex testIndex $ simpleTUSearch _hrHR sent
  res  <- parseEsResponse resp

  mapM_ showTU $ extractTUs res

  return ()

  where
    runBH' = withBH defaultManagerSettings testServer
    showTU :: (DocId, Maybe TransUnitDoc) -> BH IO ()
    showTU (DocId i, Nothing) = do putStrLn $ " ------- >> " <> i <> " - no Source"
    showTU (DocId i, Just TransUnitDoc{..}) = do
      let f TransUnitVar{..} =
               fromMaybe "*" tuvLRegion <> "-" <> fromMaybe "*" tuvLScript
                  <> " - " <> tshow tuvContent
          prt (Just tuv) = putStrLn $ i <> ": " <> f tuv
          prt Nothing    = return ()

      putStrLn " ------- >> "
      prt tudEN
      prt tudDE
      prt tudHR
      putStrLn " << ------- "


extractTUs :: Either EsError (SearchResult TransUnitDoc) -> [(DocId, Maybe TransUnitDoc)]
extractTUs r =
  case (hits . searchHits) <$> r of
    Left _   -> []
    Right xs -> map (hitDocId &&& hitSource) xs

showResp :: MonadIO m => Text -> Response LByteString -> m ()
showResp msg r = do
  let l = case statusCode $ responseStatus r of
           200 -> "OK  " <> tshow r
           201 -> "CR"
           404 -> "Not found"
           400 -> case eitherDecode (responseBody r) of
             Left e          -> "Error: " <> tshow e
             Right EsError{} -> tshow r -- unpack errorMessage
           _   -> tshow r
  liftIO $ putStrLn $ msg <> ":  " <> l


respOK :: MonadIO m => String -> Reply -> m ()
respOK m r = if isSuccess r then return () else print $ "Error - " <> m <> show r


simpleTUSearch :: Lang -> Text -> Search
simpleTUSearch Lang{..} sentence = do
  let langField = case toLower langLanguage of
                    "en" -> "tudEN"
                    "de" -> "tudDE"
                    "hr" -> "tudHR"
                    _    -> "all"
      contentField = langField <> ".tuvContent"
      tuQ1  = QueryFuzzyQuery $ FuzzyQuery { fuzzyQueryField = FieldName contentField
                                           , fuzzyQueryValue = sentence
                                           , fuzzyQueryPrefixLength = PrefixLength 0
                                           , fuzzyQueryMaxExpansions = MaxExpansions 1
                                           , fuzzyQueryFuzziness = Fuzziness 10
                                           , fuzzyQueryBoost = Nothing
                                           }
      -- tuQ2 = QueryMatchQuery $ mkMatchQuery (FieldName contentField) (QueryString sentence)
      -- tuQ3 = QueryMatchQuery $ MatchQuery
      --   { matchQueryField              = FieldName contentField
      --   , matchQueryQueryString        = QueryString sentence
      --   , matchQueryOperator           = And
      --   , matchQueryZeroTerms          = ZeroTermsNone
      --   , matchQueryCutoffFrequency    = Nothing
      --   , matchQueryMatchType          = Just MatchPhrase
      --   , matchQueryAnalyzer           = Nothing
      --   , matchQueryMaxExpansions      = Nothing
      --   , matchQueryLenient            = Nothing
      --   , matchQueryBoost              = Nothing
      --   , matchQueryMinimumShouldMatch = Just "20%"
      --   }
  mkSearch (Just tuQ1) Nothing


-- {"query":{"fuzzy":{"tudHR.tuvContent":{"value":"hrvatski","fuzziness":10,"prefix_length":0,"max_expansions":1}}}}
