module StoreTo.File where

import           Imports

import qualified Data.ByteString.Lazy           as BL


import           Application
import           Text.XML.SAXCombinators
import           Text.XML.SAXCombinators.Hexpat ()
import           Tmx.Parser


type FileState = TmxState (Maybe Int)

data OutputFormat = OFHtml | OFClean

parseTmxToFile :: FilePath -> UniqTmxId -> OutputFormat -> FilePath
               -> AppM (Either PError ())
parseTmxToFile inFnm uId format outFnm = liftIO $
  withBinaryFile outFnm WriteMode $ \outh -> do
    (streamTmxUnkTo (out outh) (pack inFnm) uId =<< readEvents inFnm) >>= \case
      Left err ->
        -- putStrLn ("Error: " <> tshow err) >>
        pure (Left err)
      Right _s  ->
        -- putStrLn ("State: " <> tshow _s)   >>
        pure (Right ())

  where
    out :: Handle -> FileState -> (TmxUnknown,[IdxNode]) -> IO (Either PError FileState)
    out h (g,_s) (unk, _unknowns) = do
      let ln = case tmxTag unk of
            TmxDecl      t                 -> tshow t
            TmxTmx       t@TmxCore{}       -> tshow t
                                           <> "\n *** >> " <> tshow (unknownNodes unk)
                                           <> "\n ### >> " <> tshow _unknowns
            TmxTransUnit TransUnitCore{..} -> do
              -- "TTUU >> " <> show tuTUVs <>
                let conv f = intercalate "\n" $ map (fst . f . tuvContent) tuTUVs
                    replTag isEnd i = "<" <> bool "" "/" isEnd <> tshow i <> ">"
                case format of
                  OFHtml  -> conv toHtml
                  OFClean -> conv (replaceTags replTag)
            TmxUnknownEvent e                -> "**" <> tshow e
            TmxError     e                   -> tshow e
      let aLine = unpack ln
      BL.hPutStr h $
      -- BC.fromStrict (encode txml) <> "\n"
        -- BC.pack (show (psCounter ps) <> ". " <> show aLine) <> "\n"
        toS (". " <> aLine) <> "\n"
      Right . (g,) . Just <$> getRandomID
