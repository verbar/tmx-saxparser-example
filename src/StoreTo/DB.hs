module StoreTo.DB
  ( TmxDBState
  , Client
  , parseTmxToDb
  )
  where

import Imports

import Database.Persist.Sql           (Key, SqlPersistT)


import Application
import Backend.DB                     (TmxDb, dbStoreTmx, dbStoreTransUnit)
import Text.XML.SAXCombinators        (IdxNode)
import Text.XML.SAXCombinators.Hexpat ()
import Tmx.Parser                     (PError, Tmx (..), TmxState, TmxUnknown (..),
                                       PError (..), streamTmxUnkTo)


type TmxDBState = TmxState (Maybe (Key TmxDb))
type Client     = Text

storeTXML :: Client -> TmxDBState -> (TmxUnknown,[IdxNode])
          -> SqlPersistT IO (Either PError TmxDBState)
storeTXML cl s unk = case fst (first tmxTag unk) of
  TmxTransUnit t -> case snd s of
    Just k  -> dbStoreTransUnit k t >> pure (Right s)
    Nothing -> pure $ err "Missing TMX parent key"
  TmxTmx t -> either (err . tshow) (Right . (fst s,) . Just) <$> dbStoreTmx cl t
  TmxDecl d -> do
    dbgM $ "DECL: " <> show d
    pure (Right s)
  TmxUnknownEvent u -> do
    dbgM $ "UNKNOWN: " <> show u
    pure (Right s)
  TmxError   e -> do
    dbgM $ "ERROR: " <> show e
    return . err $ "Error happened: " <> tshow e
  where
    err = Left . StoreError

parseTmxToDb :: Client -> FilePath -> AppM (Either PError TmxDBState)
parseTmxToDb cl fNm = do
  runDB $ streamTmxUnkTo (storeTXML cl) "DB test title" 123 =<< liftIO (readEvents fNm)
