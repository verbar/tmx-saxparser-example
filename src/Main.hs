module Main where

import Imports

import Database.V5.Bloodhound (IndexName (..), defaultIndexDocumentSettings)
import Options.Applicative
    ( execParser
    , fullDesc
    , header
    , help
    , helper
    , info
    , long
    , metavar
    , progDesc
    , short
    , showDefault
    , strOption
    , value
    )

import Application            (AppM, mkApp, runApp)
import Import.Exceptions      (AppException (..))
import StoreTo.DB             (Client, TmxDBState, parseTmxToDb)
import StoreTo.ES             (EsState, parseTmxToEs)
import StoreTo.File           (OutputFormat (..), parseTmxToFile)
import Tmx.Parser             (PError)



outputPath :: FilePath
outputPath = "data/output.xml"


------------------------------------------------------------------------------------------
-- Main entry point
------------------------------------------------------------------------------------------

-- | Command line arguments
data Args =
  Args { argFileName :: FilePath  -- ^ file path of TMX file
       , argTest     :: Text
       } deriving (Show)

-- | Main app entry point
main :: IO ()
main = do
  Args{..} <- execParser pInfo
  app <- mkApp
  let f inFnm = case argTest of
        "db"        -> void $ testDb   inFnm
        "es"        -> void $ testEs   inFnm $ IndexName "tmx_es"
        "file-es"   -> void $ testFile inFnm OFClean outputPath
        "file-html" -> void $ testFile inFnm OFHtml  outputPath
        x           -> error $ "Argument '" <> unpack x <> "' not supported"

  res <- runApp app (f argFileName)
           `catch` (\(e::SomeException) -> do
                       return . Left $ ExcText [tshow e]
                   )
  putStrLn $ case res of
    Left e  -> "Error: " <> tshow e
    Right _ -> "All OK"

  where
    pInfo = info (args <**> helper)
                 (fullDesc <> progDesc "TMX parsing examples"
                           <> header   "TMX parsing examples")
    args = Args <$> strOption (long "tmx"  <> short 'f'
                            <> value "data/Test-doc2.tmx" <> showDefault
                            <> help "Filename to parse")
                <*> strOption ( long "test" <> short 't'
                             <> help "Test to execute"
                             <> value "file-html" <> showDefault
                             <> metavar "STRING" )

testFile :: FilePath -> OutputFormat -> FilePath -> AppM (Either PError ())
testFile inFnm format outFnm = do
  parseTmxToFile inFnm 1 format outFnm

testEs :: FilePath -> IndexName -> AppM (Either PError EsState)
testEs inFnm idx = do
  let ids = defaultIndexDocumentSettings
      uId = 1
  parseTmxToEs inFnm uId ids idx

testDb :: FilePath -> AppM (Either PError TmxDBState)
testDb fNm = do
  let client = "Test client" :: Client
  parseTmxToDb client fNm
