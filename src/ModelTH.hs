{-# LANGUAGE DataKinds, StandaloneDeriving, TemplateHaskell, UndecidableInstances #-}
module ModelTH where

import Imports

import Database.Persist.Quasi
import Database.Persist.TH

import Tmx.Parser             (IsDef, TransUnitID)

-- You can define all of your database entities in the entities file.
-- You can find more information on persistent and how to declare entities
-- at:
-- http://www.yesodweb.com/book/persistent/
share [mkPersist sqlSettings, mkMigrate "migrateAll", mkDeleteCascade sqlSettings]
    $(persistManyFileWith lowerCaseSettings [ "config/common-models"
                                            , "config/tmx-models"
                                            ]
     )


instance Store ClientDb
