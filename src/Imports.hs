module Imports
  ( module X
  , tryError
  , getRandomID

  , fst3, snd3, thd3
  , prompt
  , readEvents

  , (!!?)
  )
where

import ClassyPrelude              as X hiding (catch, catchIO, catchJust, catches, handle,
                                        mapM_, replicateM, try, tryIO)
import Prelude                    as X (Read (..))

import Control.Arrow              as X ((+++), (|||))
import Control.Error              as X (failWith, hoistEither, hush, note, (??))
import Control.Exception.Safe     as X (MonadCatch, MonadThrow, catch, catchIO, catchJust,
                                        catches, handle, throw, try, tryIO)
import Control.Monad              as X (foldM_)
import Control.Monad.Error.Class  as X
import Control.Monad.Trans.Except as X hiding (liftCallCC, liftListen, liftPass)
import Control.Monad.Trans.Reader as X hiding (ask, asks, liftCallCC, liftCatch)
import Control.Monad.Trans.State  as X hiding (liftCallCC, liftCatch, liftListen,
                                        liftPass)

import Data.Aeson                 as X (FromJSON (..), ToJSON (..), Value (..), object,
                                        (.=))
import Data.Aeson.Types           as X (KeyValue, Pair)
import Data.Data                  as X hiding (DataType)
import Data.Default               as X
import Data.Either                as X
import Data.EitherR               as X
import Data.Foldable              as X (foldrM, mapM_)
import Data.Store                 as X
import Data.String.Conv           as X
import Data.Text                  as X (justifyLeft, justifyRight)
import Data.Time.Clock            as X
import Database.Persist.Class     as X (PersistField)
import Database.Persist.Sql       as X (PersistFieldSql (..), entityKey, entityVal)
import System.Mem                 as X
import System.Random              as X

import Data.ByteString.Lazy       as BL
import Text.XML.Expat.SAX         (SAXEvent (..), XMLParseLocation, defaultParseOptions,
                                   parseLocations)

import Debug                      as X

{-# INLINE fst3 #-}
fst3 :: (a,b,c) -> a
fst3 (x,_,_) = x

{-# INLINE snd3 #-}
snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x

{-# INLINE thd3 #-}
thd3 :: (a,b,c) -> c
thd3 (_,_,x) = x


prompt :: Text -> IO ()
prompt msg = do
  say $ msg <> "   >>  Press a key.."
  _ <- getLine
  return ()

tryError :: MonadError e m => m a -> m (Either e a)
tryError m = (Right <$> m) `catchError` (return . Left)

getRandomID :: IO Int
getRandomID = getStdRandom $ randomR (1,1000000)

-- | Convert Maybe inside Except to Exception
(!!?) :: (Monad m) => ExceptT e m (Maybe b) -> e -> ExceptT e m b
(!!?) a e = maybe (throwE e) pure =<< a

-- | Read xml file, stream parse with Hexpat and return the list of Events
readEvents :: FilePath -> IO [(SAXEvent Text Text, XMLParseLocation)]
readEvents fNm = parseLocations defaultParseOptions <$> BL.readFile fNm
