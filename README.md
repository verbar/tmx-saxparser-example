# Example app using tmx-saxparser - PoC

These examples can be built via `stack build`.
To run them start an Elasticsearch 6.4.x server and PostgreSQL rdbms. DB name and credentials should be changed.

Run with `stack exec tmx-saxparser-example -t action -file filename` and specify the type of action

  - db        - store translation unit variants to DB

  - es        - store translation unit variants to DB

  - file-es   - produce ElasticSearch compatible TUVs with tags replaced by "<n>"

  - file-html - produce HTML verison TUVs with tags converted to HTML representation (default)

Default filename is "data/Test-doc2.tmx"
